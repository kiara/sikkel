{ config, lib, pkgs, ... }:

{
  imports = [
    (import <mobile-nixos/lib/configuration.nix> { device = "pine64-pinephone"; })
    ./hardware-configuration.nix
    # <mobile-nixos/examples/phosh/phosh.nix>
    <mobile-nixos/examples/plasma-mobile/plasma-mobile.nix>
  ];

  networking.hostName = "mobile-nixos";

  #
  # Opinionated defaults
  #
  
  # Use Network Manager
  networking.wireless.enable = false;
  networking.networkmanager.enable = true;
  
  # Use PulseAudio
  hardware.pulseaudio.enable = true;
  
  # Enable Bluetooth
  hardware.bluetooth.enable = true;
  
  # Bluetooth audio
  hardware.pulseaudio.package = pkgs.pulseaudioFull;
  
  # Enable power management options
  powerManagement.enable = true;
  
  # It's recommended to keep enabled on these constrained devices
  zramSwap.enable = true;

  services.xserver.displayManager.autoLogin.user = "kiara";

  # Auto-login for phosh
  services.xserver.desktopManager.phosh = {
    user = "kiara";
    # for better compatibility with x11 applications
    phocConfig.xwayland = "immediate";
  };

  #
  # User configuration
  #
  
  users.users."kiara" = {
    isNormalUser = true;
    description = "kiara";
    hashedPassword = "$6$GmzjsUkfTjnEp2n9$eW84AxPlPQcUARRfxhZ80YWGKnnc4yiRmsCFHvicaqxt4KPjDIHXMzbtP90NF8fkvt2I0zboiXDui/yVZ5T29.";
    extraGroups = [
      "dialout"
      "feedbackd"
      "networkmanager"
      "video"
      "wheel"
    ];
    packages = with pkgs; [
      git
      gnupg
      kate
      dolphin
      vim
      firefox-wayland
      # gnome-console       # Terminal
      megapixels          # Camera
      chatty              # sms
      emacs
      lapce
      pass
      keepassxc
      thunderbird
      # signal-desktop # does not yet support aarch64-linux
      evince
      libreoffice-fresh
      zsh
      nextcloud-client
      # gnome.gnome-maps
      marble
      kdeconnect
      # zoom-us # does not yet support aarch64-linux
      webtorrent_desktop
      stremio
      thefuck
      squeekboard
    ];
  };

  virtualisation.waydroid.enable = true;

  programs.calls.enable = true;
  # Optional but recommended. https://github.com/NixOS/nixpkgs/pull/162894
  systemd.services.ModemManager.serviceConfig.ExecStart = [
    "" # clear ExecStart from upstream unit file.
    "${pkgs.modemmanager}/sbin/ModemManager --test-quick-suspend-resume"
  ];

  # GPS: https://github.com/NixOS/nixpkgs/pull/129630
  services.geoclue2.enable = true;
  users.users.geoclue.extraGroups = [ "networkmanager" ];

  # Sensors
  hardware.sensor.iio.enable = true;
  hardware.firmware = [ config.mobile.device.firmware ];

  # Bluetooth
  mobile.boot.stage-1.firmware = [
    config.mobile.device.firmware
  ];

  # Modem firmware
  services.fwupd.enable = true;

  programs.zsh = {
    enable = true;
    # autocd = true;
    # enableCompletion = true;
    # enableAutosuggestions = true;
    # syntaxHighlighting.enable = true;
    # historySubstringSearch.enable = true;
    ohMyZsh = {
      enable = true;
      # https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins
      plugins = [
        "git"
        "thefuck"
      ];
      theme = "agnoster";
    };
    # sessionVariables = {
    #   GTK_THEME = "palenight";
    #   EDITOR = "vim";
    #   VISUAL = "vim";
    # };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
